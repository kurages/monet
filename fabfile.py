from fabric.api import *
from fabtools import require
import re


#env.hosts = ['10.8.0.10','10.8.0.11','10.8.0.12','10.8.0.13','10.8.0.14','10.8.0.15','10.8.0.16']
#env.hosts = ['10.8.0.14','10.8.0.15','10.8.0.16']
#env.hosts = ['10.8.0.9']
#env.hosts = ['10.8.0.11']
env.user   = "media"
env.password = "(Mdmdmd)!"

def screenshot():
    output = local('mysql -D monetov -u monetov -e "SELECT terminals.id_terminal, terminals.internal FROM terminals;" -p9che86hRHf -B -N', capture=True)
    hosts = output.split('\n')
    for host in hosts:
        execute(screenshot_work, hosts=host.split()[1], id=host.split()[0])

def screenshot_work(id):
    try:
        run('uname -s')
    except:
        local("rsync -a /root/test/screen.png /var/www/www-root/data/www/admin.monetov.com/storage/app/public/screenshot_terminals/"+id+"/")
    else:
        require.deb.package('scrot')
        run("mkdir -p /home/media/sync/Screenshots/"+id)
        run("DISPLAY=:0 scrot 'screen.png' -e 'mv $f /home/media/sync/Screenshots/"+id+"/'")
        get('/home/media/sync/Screenshots/*','/var/www/www-root/data/www/admin.monetov.com/storage/app/public/screenshot_terminals/')
        run('rm -rf /home/media/sync/Screenshots/*')

def upload_coin():
    put('/root/test/coins/housemusic/*.png','/usr/share/nginx/html/_project/files/coins/5/', use_sudo=True, mode=0755 )

@hosts('10.8.0.7')
def update_right():
    put('~/test/right.sh', '/var/monetov/sync/', use_sudo=True, mode=0755)
    put('~/test/id_pointer.sh', '/var/monetov/sync/', use_sudo=True, mode=0755)
    put('~/test/99-calibration.conf', '/usr/share/X11/xorg.conf.d/', use_sudo=True, mode=0755)
    sudo('DISPLAY=:0 /var/monetov/sync/right.sh')

def upload_script():
    upload = put('~/test/ping_test.py','/home/media')

def test():
    port_number = prompt("Which port?", default=42, validate=int)
    print(port_number)

def php5():
    put('./reboot_phpfpm.sh','/home/media/')
    put('./ProjectManager.php','/home/media/')
    sudo('mv /home/media/reboot_phpfpm.sh /usr/share/nginx/html/')
    sudo('mv /home/media/ProjectManager.php /usr/share/nginx/html/_framework/php/class/')
    sudo('chmod +x /usr/share/nginx/html/reboot_phpfpm.sh')

def wvdial():
    put('wvdial.conf','/etc/', use_sudo=True, mode=0755)

def dns():
    sudo("echo -e 'dns-nameservers 8.8.8.8 8.8.4.4\nauto lo\niface lo inet loopback\niface wwan0 inet dhcp' > /etc/network/interfaces")
    sudo('/etc/init.d/networking restart')


def get_ping():
    #get('/usr/local/bin/ping_test.py','ping_test.py')
    #get('/usr/local/bin/vpn_check.sh','vpn_check.sh')
    get('/home/media/start_*.sh','./')

def put_ping():
    sudo('rm -rf /usr/local/bin/ping_test.py')
    sudo('rm -rf /usr/local/bin/vpn_check.sh')
    put('ping_test.py','/home/media/')
    put('vpn_check.sh','/home/media/')
    sudo('mv /home/media/ping_test.py /usr/local/bin/')
    sudo('mv /home/media/vpn_check.sh /usr/local/bin/')
    sudo('rm -rf /home/media/ping_test.py')
    sudo('rm -rf /home/media/vpn_check.sh')
    sudo('chmod +x /usr/local/bin/*')

    require.deb.package('lm-sensors')
    sudo("yes '' | sensors-detect")
    cron=sudo("crontab -l")
    if 'ping_test.py' not in cron:
        sudo('(crontab -l 2>/dev/null; echo "*/5 * * * * /usr/bin/python /usr/local/bin/ping_test.py >> /var/log/ping_test_print.log") | crontab -')
    if 'vpn_check.sh' not in cron:
        sudo('(crontab -l 2>/dev/null; echo "*/1 * * * * /usr/local/bin/vpn_check.sh") | crontab -')
    sudo('/etc/init.d/cron restart')
    put('start_*.sh','/home/media/')
    sudo('chmod +x /home/media/start_*.sh')

def reboot():
    sudo('reboot')
