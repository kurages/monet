#!/bin/bash

ProcName="openvpn" # указываем имя процесса openvpn
PingHost="10.8.0.1" # указываем хост, который доступен только если поднят OpenVPN канал
Check=`pidof $ProcName` # Команда для проверки запущен ли процесс OpenVPN

echo -ne "nameserver 8.8.8.8\nnameserver 127.0.1.1\n" > /etc/resolv.conf
ip route del 0.0.0.0/1 dev tun0

StartVPN()
{
        /etc/init.d/openvpn start # Стартуем процесс
}

RestartVPN()
{
        /etc/init.d/openvpn restart # Рестартим процесс
}

if [ "$Check" = "" ]
then
        # Если процесс не запущен, то стартуем VPN
        StartVPN
else
        # Если процесс запущен, то проверяем, доступен ли сервер VPN, если нет, рестартим.
        count=$(ping -c 1 $PingHost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
        if ! [ $count -eq 1 ]
        then
                RestartVPN
        fi
fi