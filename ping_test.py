#!/usr/bin/python
# -*- coding:utf-8 -*-
import time,subprocess,re,requests,traceback

def log_file(text):
    with open("/var/log/ping_test.log", "a") as file:
        file.write('['+time.strftime('%Y-%m-%d %H:%M:%S')+'] '+str(text)+'\n')

dns = subprocess.check_output(["cat", "/etc/resolv.conf"])
if '8.8.8.8' not in dns:
    log_file('Add 8.8.8.8 in resolv.conf')
    subprocess.Popen("echo 'nameserver 8.8.8.8' >> /etc/resolv.conf", shell=True)

id = subprocess.check_output(["cat", "/var/monetov/id"]).strip()
log_file('ID: '+id)

log_file('Add route...')
subprocess.call("route add default ppp0", shell=True)

log_file('Network test...')
proc_f = subprocess.Popen("ping -c 3 8.8.8.8 2<&1| grep -icE 'unknown|expired|unreachable|time out|100% packet loss'", stdout=subprocess.PIPE, shell=True)
count_f = int(proc_f.communicate()[0])
if count_f>0:
    log_file('Ping 8.8.8.8 - Fail!')
    log_file('Start wvdialconf')
    subprocess.call('wvdialconf', shell=True)
    with open('/etc/wvdial.conf','r') as wvd:
        temp_text=wvd.read()
    usb=re.compile('Modem = (.+)').findall(temp_text)[0]
    log_file('Found USB: '+str(usb))
    log_file('Reset modem and wait 30 sec')
    subprocess.call("echo -ne 'at^reset\n\r' > "+usb, shell=True)
    time.sleep(30)
    log_file('Second network test...')
    proc_f2 = subprocess.Popen("ping -c 3 8.8.8.8 2<&1| grep -icE 'unknown|expired|unreachable|time out|100% packet loss'", stdout=subprocess.PIPE, shell=True)
    count_f2 = int(proc_f2.communicate()[0])
    if count_f2>0:
        log_file('Ping 8.8.8.8 - Fail!')
    else:
        log_file('Ping 8.8.8.8 - Success!')
else:
    log_file('Ping 8.8.8.8 - Success!')

log_file('OpenVPN test...')
proc_vpn = subprocess.Popen("ping -c 3 10.8.0.1 2<&1| grep -icE 'unknown|expired|unreachable|time out|100% packet loss'", stdout=subprocess.PIPE, shell=True)
count_vpn = int(proc_vpn.communicate()[0])
if count_vpn>0:
    log_file('Ping 10.8.0.1 - Fail!')
else:
    log_file('Ping 10.8.0.1 - Success!')

ip = subprocess.check_output(["hostname", "-I"]).strip().split()
local_ip,external_ip=None,None
if len(ip) >= 2:
    for i in ip:
        if i.find('10.8.')==0:
            local_ip=i
            ip.pop(ip.index(i))
            external_ip=", ".join(ip)
            break
elif len(ip)==1:
    external_ip=ip[0]
    local_ip=None

log_file('Local IP: '+local_ip)
log_file('External IP: '+external_ip)

sens = subprocess.check_output("sensors")
try:
    temp=re.compile('Core 0:\s+(.+?)\s+\(').findall(sens)[0]
except:
    temp='Error'
log_file('Core temp: '+temp)

send_ard=''
send_ict=''
devices=subprocess.check_output("ps aux | grep -E 'ICT|ARD'", shell=True)
ard=1 if '/usr/share/nginx/html/_framework/exe/ArduinoMonetov/ARD' in devices else 0
ict=1 if '/usr/share/nginx/html/_framework/exe/ICT/ICT' in devices else 0
if not ard:
    send_ard='ARD restart'
    log_file(send_ard)
    subprocess.call("sudo su media -c '/home/media/start_ARD.sh'", shell=True)
if not ict:
    send_ict='ICT restart'
    log_file(send_ict)
    subprocess.call("sudo su media -c '/home/media/start_ICT.sh'", shell=True)

try:
    a = subprocess.check_output(['pidof', 'mysqld'])
except:
    subprocess.call(["service", "mysqld", "start"])
    log_file('Starting mysqld')

try:
    a = subprocess.check_output(['pidof', 'nginx'])
except:
    subprocess.call(["service", "nginx", "start"])
    log_file('Starting nginx')

try:
    a = subprocess.check_output(['pidof', 'php5-fpm'])
except:
    subprocess.call(["service", "php5-fpm", "start"])
    log_file('Starting php5-fpm')

log_file('Sending POST...')

data={'id': id,'time': time.strftime('%Y-%m-%d %H:%M:%S'),'local_ip': local_ip,'external_ip': external_ip,'temp': temp,'ard': send_ard, 'ict': send_ict}
try:
    if local_ip:
        r = requests.post("http://10.8.0.1/terminal?hash=IQz0y0P1L90O0gy4Eyba", data=data, timeout=20)
    else:
        r = requests.post("http://admin.monetov.com/terminal?hash=IQz0y0P1L90O0gy4Eyba", data=data, timeout=20)
    log_file('Request: ' + str(r.text))
except:
    log_file('Cannot send!')
    log_file(traceback.format_exc())

log_file('~'*20)